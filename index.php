<?php

require_once 'vendor/autoload.php' ;
session_start();
$app = new \Slim\Slim ;
use Illuminate\Database\Capsule\Manager as DB;

$config = parse_ini_file('src/conf/conf.ini');
$db = new DB();
$db->addConnection( $config );
$db->setAsGlobal();
$db->bootEloquent();

$app->get('/', function(){
  $app =\Slim\Slim::getInstance() ;
  $app->redirect($app->urlFor('connexion'));
})->name('racine');

$app->get('/Accueil', function(){
  $control= new \justjob\controleurs\controleur();
  $control->getAccueil();
})->name('accueil');

$app->get('/Connexion/creeCompte',function(){
  $control= new \justjob\controleurs\controleur();
  $control->getCreationCompte();
})->name('inscription');

$app->post('/Connexion/creeCompte',function(){
  $control= new \justjob\controleurs\controleur();
  $control->creationCompte(password_hash($_POST['mdp'],PASSWORD_DEFAULT),$_POST['login'],$_POST['nom']);
});

$app->get('/Connexion/seConnecter',function(){
  $control= new \justjob\controleurs\controleur();
  $control->getConnexion();
})->name('connexion');

$app->post('/Connexion/seConnecter',function(){
  $control= new \justjob\controleurs\controleur();
  $control->connexion($_POST['login'],$_POST['mdp']);
});

$app->get('/CreationEmploi',function(){
  $control= new \justjob\controleurs\CreationControleur();
  $control->getCreerEmploi();
})->name('creerEmploi');



$app->run();
