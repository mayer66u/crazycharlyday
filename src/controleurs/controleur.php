<?php
namespace justjob\controleurs;
use justjob\modeles\User as User;

class controleur{

  public function getAccueil(){
       $v = new \justjob\vues\vue();
       $v->render(1);
   }

   public function getConnexion($t=[]){
    $v = new \justjob\vues\vue($t);
    $v->render(2);
  }

  public function connexion($login,$mdp){
    $app =\Slim\Slim::getInstance() ;
    $user = User::where("login","=",$login)->first();
    if ($user != null) {
      if (password_verify($mdp, $user->mdp)) {
		  $_SESSION["login"]=$login;
		  //$_SESSION["mdp"]=$user->mdp;
		  setCookie("mwl",$user->login, time()+3600*24*365,"/justjob");
        $app->redirect($app->urlFor('accueil'));
      }else {
        $this->getConnexion($t = array('msg' => "Login ou mot de passe incorrect")); //Mot de passe incorrect
      }
    }else {
      $this->getConnexion($t = array('msg' => "Login ou mot de passe incorrect ")); //Compte inexistant
    }
  }

  public function getCreationCompte($t=[]){
    $v = new \justjob\vues\vue($t);
    $v->render(3);
  }

  public function creationCompte($mdp,$login,$nom){
    $app =\Slim\Slim::getInstance();
    $log = User::where("login","=",$login)->first();
    if($log != null){
      $this->getCreationCompte($t = array('msg' => "Login déjà existant"));
    }
    else{
      $user = new User();
      $user->mdp = $mdp;
      $user->login = $login;
      $user->nom = $nom;
      $user->save();
      $_SESSION['login']=$login;
		  //$_SESSION['mdp']=$user->mdp;
		  setCookie('mwl',$user->login, time()+3600*24*365,"/justjob");
      $app->redirect($app->urlFor('accueil'));
    }
  }

}
