<?php
namespace justjob\controleurs;
use justjob\modeles\Emploi as Emploi;

class CreationControleur{

  public function getCreerEmploi($t=[]){
   $v = new \justjob\vues\VueCreationEmploi($t);
   $v->render(1);
  }

  public function creerEmploi($id,$profil,$duree,$lieu,$etat,$categorie){
    $emploi = new Emploi();
    $emploi->id = $id;
    $emploi->profil = $profil;
    $emploi->duree = $duree;
    $emploi->lieu = $lieu;
    $emploi->etat = $etat;
    $emploi->categorie = $categorie;
    $emploi->save();
  }

}
