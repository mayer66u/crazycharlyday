<?php
namespace justjob\vues;

class vue{

  private $tab,$header,$background;

	 public function __construct($t=[]){
		$this->tab=$t;
		$this->header = "";
		$this->background = "";
	}

  private function accueilSite(){
      $this->header = "<link type='text/css' rel='stylesheet' href='css/materialize.min.css'   media='screen,projection'/>";
      $this->background ="<div class='card-panel brown lighten-2'</div>";
      return <<<END
      <div class="container">
   <div class="slider">
    <ul class="slides">
      <li>
        <img class="z-depth-5" src="img/pote.jpg"> 
        <div class="caption center-align">
          <h3>Bienvenue sur le site <</h3>
          <h5 class="light blue-text text-lighten-3">Votre différence est votre force, trouvez un emploi sur notre site</h5>
        </div>
      </li>
      <li>
        <img id ="img" src="img/handicap.jpg"> 
        <div class="caption left-align">
          <h3 class ="red-text darken-2"> Recrutement sur CV</h3>
          <h5 class="light grey-text black"></h5>
        </div>
      </li>
      <li>
        <img id ="img" src="img/benze.jpg"> 
        <div class="caption right-align">
          <h3>Ici est le site de votre vie</h3>
          <h5 class="light grey-text text-lighten-3">Malgrès votre handicap, trouvez un emploi !!!.</h5>
        </div>
      </li>
      <li>
        <img id ="img" src="img/steph.jpg"> 
        <div class="caption center-align">
          <h3>Tout est possible sur note site !!!</h3>
          <h5 class="light grey-text text-lighten-3">Pas de discrimination </h5>
        </div>
      </li>
    </ul>
  </div>
</div>
END;

}
  private function afficheConnexion(){

    $this->header = "<link type='text/css' rel='stylesheet' href='../css/materialize.min.css'  media='screen,projection'/>";
    $app =\Slim\Slim::getInstance();
    $urlI = $app->urlFor('inscription');
      $urlC = $app->urlFor('connexion');
    $msg = '';
    if (isset($this->tab['msg'])){
      $msg = $this->tab['msg'];
    }

    return <<<END
    <a href='$urlC'><div class="card-panel amber darken-1 grey-text text-darken-4"> <h1>JustJob</h1> </div></a>
		<h4>Êtes-vous déjà membre ?</h4>

    <p>$msg</p><br>
		<form action = "" method = "POST">
			Login : <input type = "text" name = "login" autofocus required> <br><br>
			Mot de passe : <input type = "password" name = "mdp" required> <br><br><br>
			<input type = "submit" value = "Se Connecter" class = "bouton">
		</ form>

		<h4>Rejoignez-nous :</h4>

		<a href='$urlI'><input type="button" class="bouton" style="width:120px" value="Créer un compte"/>
END;
  }

  private function afficheCreationCompte(){
      $app =\Slim\Slim::getInstance();
      $urlC = $app->urlFor('connexion');
    $this->header = "<link type='text/css' rel='stylesheet' href='../css/materialize.min.css'  media='screen,projection'/>";
    $msg = '';
    if (isset($this->tab['msg'])){
      $msg = $this->tab['msg'];
    }

    return <<<END
    <a href='$urlC'><div class="card-panel amber darken-1 grey-text text-darken-4"> <h1>JustJob</h1> </div></a>
        <script type="text/javascript">
    		<!--

    		function verif_formulaire()
    		{
    		 if(document.formulaire.mdp.value != document.formulaire.cmdp.value)  {
    		   alert("Veuillez entrer le même mot de passe");
    		   document.formulaire.cmdp.focus();
    		   return false;
    		  }
    		}
    		//-->
    		</script>
        
        <p>$msg</p><br>

        <form name="formulaire" action = "" method = "POST" onSubmit="return verif_formulaire()">
          Login : <input type = "text" name = "login" autofocus required> <br><br>
          Nom : <input type = "text" name = "nom" required> <br><br>
					Mot de passe : <input type = "password" name = "mdp" required> <br><br>
          Confirmer le mot de passe : <input type = "password" name = "cmdp" required> <br><br><br>
					<input type = "submit" value = "Valider" class = "bouton">
				</ form>
END;
}

	public function render($sel){


		switch($sel){
			case 1:
				$content = $this->accueilSite();
				break;
      case 2:
        $content = $this->afficheConnexion();
        break;
      case 3:
        $content = $this->afficheCreationCompte();
        break;

			default:

			break;
		}

		 $html = <<<END
			<!DOCTYPE html>
			<html $this->background
			<head>
	      <title>JustJob</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
				$this->header
		<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	      <meta charset="utf-8"/>
	    </head>
	    <body>
	    <script>
    		<!--

    		$(document).ready(function(){
      $('.slider').slider();
    });
    		//-->
    		</script>
		

			<div class="content">
				$content
			</div>

			</body></html>
END;

    echo $html;
	}

}
