<?php
namespace justjob\vues;

class VueCreationEmploi{


private $tab,$header;

	 public function __construct($t=[]){
		$this->tab=$t;
		$this->header = "";
	}


public function afficherFormuCreationEmploi()
    {
			$this->header = "<link type='text/css' rel='stylesheet' href='css/materialize.min.css'  media='screen,projection'/>";
        $res = <<<END
        <div class="formulaireCreaEmploi">
            <h2>CRÉATION D'UN EMPLOI</h2>
            <form method="post" action="" enctype="multipart/form-data">
                <label for="profil"> PROFIL DE POSTE :</label>
                <input type="text" name="profil" id="profil" size="30" maxlength="30" autofocus required>

                <label for="duree"> DUREE :</label>
                <input type="text" name="duree" required>
                <br /><br />

                <label for="lieu"> LIEU DE TRAVAIL :</label>
                <input type="text" name="lieu" id="lieu" size="30" maxlength="30" autofocus required>


                <input type="radio" name="etat" value="1"/>
                <span for="etat">ACTIF</span>
                <input type="radio" name="etat" value="0"/>
                <span for="etat">INACTIF</span>

                <label for="categorie"> CATEGORIE ACTIVITÉ :</label>
                <input type="text" name="categorie" id="categorie" size="30" maxlength="30" autofocus required>

                <input type="submit" value="Valider">
                <br />
            </form>
        </div>
END;
        return $res;
    }



    	public function render($sel){
            $app =\Slim\Slim::getInstance();
            $urlC = $app->urlFor('accueil');

            switch($sel){
                case 1:
				    $content = $this->afficherFormuCreationEmploi();
				    break;



                default:

			         break;
		  }

		 $html = <<<END
			<!DOCTYPE html>
			<html>
			<head>
	      <title>JustJob</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
				$this->header
        <script type="text/javascript" src="js/materialize.min.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	      <meta charset="utf-8"/>
	    </head>
			<body>
      <a href='$urlC'><div class="card-panel amber darken-1 grey-text text-darken-4"> <h1>JustJob</h1> </div></a>

			<div class="content">
				$content
			</div>

			</body></html>
END;

    echo $html;
	}

}
