<?php
 namespace justjob\modeles;
 Use \Illuminate\Database\Eloquent\Model as Model;

class User extends Model{

    protected $table = "user";
    protected $primaryKey = "id";
    public $timestamps = false;

public function categorie(){

    return $this->belongsTo('\crazycharlyday\modeles\Categorie');
}
}
